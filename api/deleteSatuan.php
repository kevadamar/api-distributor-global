<?php
include  '../config/functions.php';
include './constans.php';

$id = $_POST['unit_id'];

$query = "DELETE FROM `$units`  WHERE `unit_id` = '$id'";

$responses = array("code" => null, "data" => null, "message" => null);

$resultToken = checkToken();

if ($resultToken['data']->role == 2) {
    header('Content-Type: application/json', true, 400);
    $responses['code'] = 400;
    $responses['message'] = "Akses Ditolak kecuali Admin";
    echo json_encode($responses);
    return;
}

if ($resultToken['status']) {

    if (get("SELECT COUNT(*) AS count FROM `$units` WHERE unit_id = '$id'")->count == 0) {
        header('Content-Type: application/json', true, 404);
        $responses["code"] = 404;
        $responses["message"] = "Data Tidak Data";
        echo json_encode($responses);
        return;
    }

    $hasil = mysqli_query($con, $query);

    if ($hasil) {
        header('Content-Type: application/json', true, 200);
        $responses["code"] = 200;
        $responses["message"] = "Berhasil Hapus Data";
    } else {
        header('Content-Type: application/json', true, 400);
        $responses["code"] = 400;
        $responses["message"] = "Gagal Hapus Data";
    }
} else {
    header('Content-Type: application/json', true, $resultToken['code']);

    $responses['code'] = $resultToken['code'];
    $responses['message'] = $resultToken['msg'];
}


echo json_encode($responses);

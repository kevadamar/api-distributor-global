<?php

// Atur jenis response
include '../config/functions.php';

use Firebase\JWT\JWT;

header('Content-Type: application/json');

// Cek method request
if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
    http_response_code(405);
    exit();
}

$headers = getallheaders();

// Periksa apakah header authorization-nya ada
if (!isset($headers['Authorization'])) {
    http_response_code(401);
    exit();
}

// Mengambil token
list(, $token) = explode(' ', $headers['Authorization']);

try {
    // Men-decode token. Dalam library ini juga sudah sekaligus memverfikasinya
    $result = JWT::decode($token, $_ENV['ACCESS_TOKEN_SECRET'], ['HS256']);

    $games = [
        'code' => 200,
        'success' => true,
        'data' => $result
    ];


    echo json_encode($games);
} catch (Exception $e) {
    // Bagian ini akan jalan jika terdapat error saat JWT diverifikasi atau di-decode
    http_response_code(401);
    // print_r($e);
    echo json_encode(
        [
            'code' => 401,
            'err' => "Unauthorized"
        ]
    );
    // exit();
}
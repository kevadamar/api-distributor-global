<?php

include '../config/functions.php';

use Firebase\JWT\JWT;

// header('Content-Type: application/json');

$email = $_POST['email'];
$password = md5($_POST['password']);

// echo json_encode([
//     'data' => $password
// ]);
// return;

$namaTable = "users";

$responses = array("code" => null, "token" => null, "message" => null);

$row = get("SELECT * FROM `$namaTable` u WHERE u.email = '$email' AND u.password = '$password'");

$jumlahRec = get("SELECT COUNT(*) AS count FROM `$namaTable` u WHERE u.email = '$email' AND u.password = '$password'")->count;
// echo json_encode([
//     $row->full_name,
//     $jumlahRec
// ]);
// return;
if ($jumlahRec > 0) {

    header('Content-Type: application/json', true, 200);
    $responses['code'] = 200;
    // $responses["exp"] = time() + (60 * 60 * 24);
    $responses["message"] = "Success Login!";
    $idusr;
    $result = true;
    // foreach ($rows as $row) {
    $idusr = (int)$row->user_id;
    // $responseField['user_id'] = $idusr;

    $token = $row->token != null ? $row->token : 0;
    $responseField['name'] = $row->full_name;
    // $responseField['password'] = $row->password;
    // $responseField['email'] = $row->email;
    // $responseField['address'] = $row->address;
    // $responseField['phone_number'] = $row->phone_number;
    $responseField['role'] = (int)$row->role;

    $responses["data"] = $responseField;

    $responseField["user_id"] = $idusr;
    $responses["dataDecode"] = $responseField;

    // array_push($responses, $responseField);
    // }
    $access_token = ($token == 0 ? JWT::encode($responses["dataDecode"], $_ENV['ACCESS_TOKEN_SECRET']) : $token);
    $responses["token"] = $access_token;

    if ($token == 0) {
        $result = mysqli_query($con, "UPDATE `$namaTable` SET token='$access_token' WHERE user_id = '$idusr'");
    }
    // echo json_encode($access_token);
    // return;
    if (!$result) {
        header('Content-Type: application/json', true, 500);

        $responses['code'] = 500;
        $responses['message'] = "Invalid Credential Internal";
        $access_token = null;
        $token = null;
    }
} else {

    header('Content-Type: application/json', true, 404);

    $responses['code'] = 404;
    $responses['message'] = "Email atau Password salah : " . $jumlahRec . " data";
    $access_token = null;
    $token = null;
}

unset($responses["dataDecode"]);

echo json_encode($responses);
$access_token = null;
$token = null;

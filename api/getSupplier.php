<?php
include '../config/functions.php';
include './constans.php';

$id = $_GET['supplier_id'];

$query = "SELECT * FROM `$suppliers` WHERE supplier_id = '$id'";

$responses = array("code" => null, "data" => null);
// $idx = 0;
$resultToken = checkToken();

if ($resultToken['data']->role == 2) {
    header('Content-Type: application/json', true, 400);
    $responses['code'] = 400;
    $responses['message'] = "Akses Ditolak kecuali Admin";
    echo json_encode($responses);
    return;
}

if ($resultToken['status']) {

    # code...
    $sql = mysqli_query($con, $query);
    $dbField = mysqli_fetch_assoc($sql);

    $responses["data"] = [
        "id" => $dbField['supplier_id'],
        "nama_supplier" => $dbField['full_name'],
        "nomor_hp" => $dbField['phone_number'],
        "alamat" => $dbField['address']
    ];


    header('Content-Type: application/json', true, 200);
    $responses["code"] = 200;
} else {
    header('Content-Type: application/json', true, $resultToken['code']);

    $responses['code'] = $resultToken['code'];
    $responses['message'] = $resultToken['msg'];
}

echo json_encode($responses);

<?php
include '../config/functions.php';
include './constans.php';

$id = $_POST['unit_id'];
$namaSatuan = $_POST['nama_satuan'];
$satuan = $_POST['satuan'];
header('Content-Type: text/xml');
$hasil;

$query = "SELECT * FROM `$units` WHERE `unit_id` = '$id'";

$responses = array("code" => null, "data" => null, "message" => null);

$resultToken = checkToken();
if ($resultToken['data']->role == 2) {
    header('Content-Type: application/json', true, 400);
    $responses['code'] = 400;
    $responses['message'] = "Akses Ditolak kecuali Admin";
    echo json_encode($responses);
    return;
}

if ($resultToken['status']) {
    # code...
    $sql = mysqli_query($con, $query);

    if (mysqli_num_rows($sql) > 0) {
        
        $namaSatuan = (isset($namaSatuan) ? $namaSatuan : mysqli_fetch_assoc($sql)['unit_name']);
        $satuan = (isset($satuan) ? $satuan : mysqli_fetch_assoc($sql)['unit']);

        $query = "UPDATE `$units` SET `unit_name` = '$namaSatuan', `unit` = '$satuan' WHERE `unit_id` = '$id'";
        $sql = mysqli_query($con, $query);
        $hasil = $sql;
    } else {
        header('Content-Type: application/json', true, 404);
        $responses["code"] = 404;
        $responses['message'] = "Data Not Found";
        echo json_encode($responses);
        return;
    }
    
    if ($hasil) {
        header('Content-Type: application/json', true, 200);
        $responses['code'] = 200;
        $responses['message'] = "Berhasil Update";
    } else {
        header('Content-Type: application/json', true, 400);
        $responses["code"] = 400;
        $responses['message'] = "Gagal Update";
    }
    
} else {
    header('Content-Type: application/json', true, $resultToken['code']);

    $responses['code'] = $resultToken['code'];
    $responses['message'] = $resultToken['msg'];
}

// echo ($responses["data"]);
echo json_encode($responses);

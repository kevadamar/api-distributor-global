<?php

include '../config/functions.php';
include './constans.php';

$idBarang = $_POST['product_id'];

$responses = array("code" => null, "data" => null, "message" => null);

$resultToken = checkToken();
$userID = $resultToken['data']->user_id;

$resultCart = get("SELECT qty,price FROM `$cart` WHERE user_id = '$userID' AND product_id = '$idBarang'");
$resultProduct = get("SELECT p.harga_jual AS price FROM `$products` p WHERE p.product_id = '$idBarang'");

$getQty = 0;
$cekHarga = 0;


if (is_null($resultCart)) {

    if (is_null($resultProduct)) {
        header('Content-Type: application/json', true, 404);
        $responses['code'] = 404;
        $responses['message'] = "Gagal Tambah Data Cart. Product Tidak Ada";
        echo json_encode($responses);
        return;
    }

    $hasil = mysqli_query($con, "INSERT INTO `$cart` VALUES(NULL,'$userID','$idBarang','1','$resultProduct->price', CURRENT_TIMESTAMP())");

    header('Content-Type: application/json', true, 200);
    $responses['code'] = 200;
    $responses['message'] = "Berhasil Tambah Data Cart";
    echo json_encode($responses);
    return;
}

$hargasatuan = $resultProduct->price;

$getQty += $resultCart->qty;
$cekHarga += ($resultCart->price + $hargasatuan);

$qty = ($getQty + 1);
$harga = ($cekHarga === 0 ? $_POST['price'] : $cekHarga);
$hasil;
if ($idBarang != 0) {
    if ($getQty > 0) {
        $hasil = mysqli_query($con, "UPDATE `$cart` SET qty = '$qty', price = '$harga' WHERE user_id = '$userID' AND product_id = '$idBarang'");
        $getQty = 0;
        $qty = 0;
    } else {
        $hasil = mysqli_query($con, "INSERT INTO `$cart` VALUES(NULL,'$userID','$idBarang','$qty','$harga', CURRENT_TIMESTAMP())");

        $getQty = 0;
        $qty = 0;
    }

    if ($hasil) {
        header('Content-Type: application/json', true, 200);
        $responses['code'] = 200;
        $responses['message'] = "Berhasil Tambah Data Cart";
    } else {
        header('Content-Type: application/json', true, 400);
        $responses['code'] = 400;
        $responses['message'] = "Gagal Tambah Data Cart";
    }
} else {
    header('Content-Type: application/json', true, 500);
    $responses['code'] = 500;
    $responses['message'] = "Internal server orror";
}

echo json_encode($responses);

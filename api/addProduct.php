<?php
include  '../config/functions.php';
include './constans.php';


$responses = array("code" => null, "data" => null, "message" => null);
$idx = 0;

$id = $_POST['id_barang'];
$id_kategori = $_POST['id_kategori'];
$id_satuan = $_POST['id_satuan'];
$supid = $_POST['supid'];

//image handler
$imgName = $_FILES['image']['name'];
$tempName = $_FILES['image']['tmp_name'];

header('Content-Type: application/json');

$resultToken = checkToken();

if ($resultToken['data']->role == 2) {
    header('Content-Type: application/json', true, 400);
    $responses['code'] = 400;
    $responses['message'] = "Akses Ditolak kecuali Admin";
    echo json_encode($responses);
    return;
}

if ($resultToken['status']) {
    $image = date('dmYHis') . str_replace(" ", "", basename($imgName));
    $imagePath = ("../uploads/$image");
    $cek = move_uploaded_file($tempName, $imagePath);
    // var_dump($imgName);

    if (!$cek) {
        header('Content-Type: application/json', true, 500);
        $responses['code'] = 500;
        $responses['message'] = "Error Save Image";
        echo json_encode($responses);
        return;
    }
    //end image handler

    $nama_barang = $_POST['nama_barang'];
    $hargaBeli = $_POST['harga_beli'];
    $hargaJual = ($hargaBeli + ($hargaBeli * (10 / 100)));
    $deskripsi = $_POST['deskripsi'];
    $stok = $_POST['stok'];


    $hasil = mysqli_query($con, "INSERT INTO `$products`(`product_id`, `category_id`, `unit_id`, `supplier_id`, `product_name`, `decription`, `image_url`, `harga_beli`, `harga_jual`, `stock`, `created_at`, `updated_at`) VALUES(NULL,'$id_kategori','$id_satuan','$supid','$nama_barang','$deskripsi','$image','$hargaBeli','$hargaJual','$stok', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP())");

    // $sql = mysqli_query($con, $query);

    if ($hasil) {
        header('Content-Type: application/json', true, 201);
        $responses['code'] = 201;
        $responses['message'] = "Berhasil simpan";
    } else {
        header('Content-Type: application/json', true, 400);
        $responses['code'] = 400;
        $responses['message'] = "Gagal simpan";
        $responses['ss'] = [
            $nama_barang,
            $hargaBeli,
            $deskripsi,
            $stok, $id_kategori,
            $id_satuan, $supid, $imgName
        ];
        echo json_encode($responses);
        return;
    }
} else {
    header('Content-Type: application/json', true, $resultToken['code']);

    $responses['code'] = $resultToken['code'];
    $responses['message'] = $resultToken['msg'];
}

echo json_encode($responses);

<?php

include '../config/functions.php';
include './constans.php';

$responses = array("code" => null, "data" => [], "total" => 0, "message" => null);
$idx = 0;

$resultToken = checkToken();
$userid = $resultToken['data']->user_id;

$rssql = "SELECT DISTINCT(a.product_id),a.user_id,SUM(a.price) AS price, SUM(a.qty) AS qty, (SELECT product_name FROM $products WHERE product_id = a.product_id) AS product_name, (SELECT image_url FROM $products WHERE product_id = a.product_id) AS gambar FROM $cart a WHERE a.user_id = '$userid' AND product_id in (SELECT product_id FROM $products) GROUP BY a.product_id ";

$sql = mysqli_query($con, $rssql);

while ($a = mysqli_fetch_array($sql)) {
    $responseField['product_id'] = $a['product_id'];
    $responseField['user_id'] = $a['user_id'];
    $responseField['product_name'] = $a['product_name'];
    $responseField['gambar'] = 'http://192.168.1.2:8888/api-distributor-global/uploads/' . $a['gambar'];
    $responseField['price'] = $a['price'];
    $responseField['qty'] = $a['qty'];
    $responses['total'] += $a['price'];
    $responses["data"][$idx] = $responseField;
    // array_push($responses, $responseField);
    $idx++;
}

// var_dump($sql);
if ($sql) {
    header('Content-Type: application/json', true, 200);
    $responses["code"] = 200;
    $responses["message"] = "Success";
} else {
    header('Content-Type: application/json', true, 500);
    $responses["code"] = 500;
    $responses["message"] = "Internal server error";
}

echo json_encode($responses);

<?php

include '../config/functions.php';
include './constans.php';

$responses = array("code" => null, "data" => null, "message" => null);

$id_barang = $_POST['product_id'];
$resultToken = checkToken();
$userid = $resultToken['data']->user_id;

if ($id_barang != 0) {
    $rssql = "SELECT qty, price FROM $cart WHERE user_id='$userid' AND product_id='$id_barang'";

    $result = get($rssql);

    $getQty = 0;
    $cekHarga = 0;
    $hargaSatuan = 0;
    $hasil;

    $sql = mysqli_query($con, $rssql);
    while ($a = mysqli_fetch_array($sql)) {
        $sqlBrg = "SELECT harga_jual AS harga FROM $products WHERE product_id='$id_barang'";
        $rssqlBrg = mysqli_query($con, $sqlBrg);
        while ($b = mysqli_fetch_array($rssqlBrg)) {
            $hargaSatuan = $b['harga'];
        }

        $getQty = $a['qty'];
        $cekHarga = ($a['price']);
    }

    $qty = ($getQty - 1);
    $harga  = ($cekHarga == 0 ? 0 : ($cekHarga - $hargaSatuan));


    if (empty($result)) {
        header('Content-Type: application/json', true, 404);

        $responses['code'] = 404;
        $responses['message'] = "not found";
        echo json_encode($responses);
        return;
    }

    if ($getQty == 0 || $getQty <= 1) {
        $hasil = mysqli_query($con, "DELETE FROM $cart WHERE user_id='$userid' AND product_id='$id_barang'");
    } else {
        $hasil = mysqli_query($con, "UPDATE $cart SET qty = '$qty', price='$harga' WHERE `user_id`='$userid' AND `product_id`='$id_barang'");
    }


    if ($hasil) {
        header('Content-Type: application/json', true, 200);
        $responses['code'] = 200;
        $responses['message'] = "Berhasil Update Data";
    } else {
        header('Content-Type: application/json', true, 400);
        $responses['code'] = 400;
        $responses['message'] = "Gagal Update Data";
    }
} else {
    header('Content-Type: application/json', true, 404);

    $responses['code'] = 404;
    $responses['message'] = "id_barang is required";
}

echo json_encode($responses);

<?php
include '../config/functions.php';
include './constans.php';

$query = "SELECT * FROM `$suppliers`";

$responses = array("code" => null, "data" => null);
$idx = 0;

$resultToken = checkToken();

if ($resultToken['data']->role == 2) {
    header('Content-Type: application/json', true, 400);
    $responses['code'] = 400;
    $responses['message'] = "Akses Ditolak kecuali Admin";
    echo json_encode($responses);
    return;
}

if ($resultToken['status']) {

    # code...
    $sql = mysqli_query($con, $query);

    while ($dbField = mysqli_fetch_assoc($sql)) {
        $responseField["id"] = $dbField['supplier_id'];
        $responseField["nama_supplier"] = $dbField['full_name'];
        $responseField["nomor_hp"] = $dbField['phone_number'];
        $responseField["alamat"] = $dbField['address'];

        $responses["data"][$idx] = $responseField;
        $idx++;
    }
    header('Content-Type: application/json', true, 200);
    $responses["code"] = 200;
} else {
    header('Content-Type: application/json', true, $resultToken['code']);

    $responses['code'] = $resultToken['code'];
    $responses['message'] = $resultToken['msg'];
}

echo json_encode($responses);

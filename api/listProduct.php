<?php
include  '../config/functions.php';
include './constans.php';
header('Content-Type: application/json', true);

// kategori id
$qparam = $_GET['q'];

// supplier id
$pid = $_GET['p'];

// search param
$qsearch = $_GET['s'];

// echo json_encode(empty($qparam));
// return;
$responses = array("code" => null, "countData" => 0, "data" => null);
$idx = 0;

$query = "SELECT `product_id`, c.`name` AS nama_kategori,u.`unit_name` AS nama_satuan,u.`unit` AS satuan , s.`full_name` AS nama_supplier, `product_name`, `decription`, `image_url`, `harga_beli`, `harga_jual`, `stock`, u.`unit_id`, c.`category_id`, s.`supplier_id` FROM `$products` p INNER JOIN `$categories` c ON p.category_id = c.category_id INNER JOIN `$units` u ON p.unit_id = u.unit_id INNER JOIN `$suppliers` s ON p.`supplier_id` = s.`supplier_id`";

// Get Detail by id kategori dan id product
if (!empty($qparam) && !empty($pid)) {
    header('Content-Type: application/json', true, 200);
    $responses["message"] = "Success";

    $query = $query . " WHERE c.category_id = '$qparam' AND p.product_id = '$pid'";
    $hasil = get($query);
    // echo json_encode($hasil);
    // return;
    $responses["code"] = (($hasil) != null ? 200 : 404);
    $responses["countData"] = ($hasil == null ? 0 : count([$hasil]));
    $responses["data"] = ($hasil);
    $responses["message"] = (($hasil) != null ? "Success" : "Products Dengan Product '" . $pid . "' dan Kategori '" . $qparam . "' Tidak Tersedia");

    echo json_encode($responses);
    return;
}

// Get list by id kategori or query search like
if (!empty($qparam) || !empty($qsearch)) {
    header('Content-Type: application/json', true, 200);
    $responses["message"] = "Success";

    $query = (!empty($qsearch) ? $query . " WHERE c.category_id = '$qparam' AND p.product_name LIKE '%$qsearch%'" : $query . " WHERE c.category_id = '$qparam'");

    $msg = (!empty($qsearch) ? "Products Dengan '" . $qsearch . "' Tidak Tersedia Dalam Pencarian" : "Products Dengan Kategori '" . $qparam . "' Tidak Tersedia");

    $hasil = getAll($query);

    $responses["code"] = ($hasil != null ? 200 : 404);
    $responses["countData"] = ($hasil == null ? 0 : count($hasil));
    $responses["data"] = ($hasil);
    $responses["message"] = ($hasil != null ? "Success" : $msg);

    echo json_encode($responses);
    return;
}

if (empty($qparam) && empty($pid)) {
    // header('Content-Type: application/json', true, 400);
    // $responses["code"] = 400;
    // $responses["message"] = "Invalid no query param url. pakai 'q' untuk get list product by kategori. pakai 'q' dan 'p' untuk get detail product by kategori dan by product id";
    $hasil = getAll($query);
    $responses["code"] = 200;
    $responses["countData"] = ($hasil == null ? 0 : count($hasil));
    $responses["data"] = $hasil;
    $responses["message"] = ($hasil != null ? "Success" : "Data Produk kosong.");

    echo json_encode($responses);
    return;
}

// delete isi object pada array yg sama
function my_array_unique($array, $keep_key_assoc = false)
{
    $duplicate_keys = array();
    $tmp = array();

    foreach ($array as $key => $val) {
        // convert objects to arrays, in_array() does not support objects
        if (is_object($val))
            $val = (array)$val;

        if (!in_array($val, $tmp))
            $tmp[] = $val;
        else
            $duplicate_keys[] = $key;
    }

    foreach ($duplicate_keys as $key)
        unset($array[$key]);

    return $keep_key_assoc ? $array : array_values($array);
}

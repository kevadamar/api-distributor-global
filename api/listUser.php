<?php
include '../config/functions.php';
include './constans.php';

$query = "SELECT * FROM `$users`";

$responses = array("code" => null, "counData" => 0, "data" => null);
$idx = 0;

$resultToken = checkToken();

if ($resultToken['data']->role == 2) {
    header('Content-Type: application/json', true, 400);
    $responses['code'] = 400;
    $responses['message'] = "Akses Ditolak kecuali Admin";
    echo json_encode($responses);
    return;
}

if ($resultToken['status']) {

    # code...
    header('Content-Type: application/json', true, 200);
    $hasil = getAll($query);
    
    $responses['data'] = $hasil;
    $responses["counData"] = count($hasil);
    $responses["code"] = 200;

} else {
    header('Content-Type: application/json', true, $resultToken['code']);

    $responses['code'] = $resultToken['code'];
    $responses['message'] = $resultToken['msg'];
}

echo json_encode($responses);

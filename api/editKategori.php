<?php
include '../config/functions.php';
include './constans.php';

$id = $_POST['category_id'];
$namaKategori = $_POST['nama_kategori'];
header('Content-Type: text/xml');
$hasil;

$query = "SELECT * FROM `$categories` WHERE `category_id` = '$id'";

$responses = array("code" => null, "data" => null, "message" => null);

$resultToken = checkToken();
if ($resultToken['data']->role == 2) {
    header('Content-Type: application/json', true, 400);
    $responses['code'] = 400;
    $responses['message'] = "Akses Ditolak kecuali Admin";
    echo json_encode($responses);
    return;
}

if ($resultToken['status']) {
    # code...
    $sql = mysqli_query($con, $query);

    if (mysqli_num_rows($sql) > 0) {
        $query = "UPDATE `$categories` SET `name` = '$namaKategori' WHERE `category_id` = '$id'";
        $sql = mysqli_query($con, $query);
        $hasil = $sql;
    } else {
        header('Content-Type: application/json', true, 404);
        $responses["code"] = 404;
        $responses['message'] = "Data Not Found";
        echo json_encode($responses);
        return;
    }
    
    if ($hasil) {
        header('Content-Type: application/json', true, 200);
        $responses['code'] = 200;
        $responses['message'] = "Berhasil Update";
    } else {
        header('Content-Type: application/json', true, 400);
        $responses["code"] = 400;
        $responses['message'] = "Gagal Update";
    }
    
} else {
    header('Content-Type: application/json', true, $resultToken['code']);

    $responses['code'] = $resultToken['code'];
    $responses['message'] = $resultToken['msg'];
}

// echo ($responses["data"]);
echo json_encode($responses);

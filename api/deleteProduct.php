<?php
include  '../config/functions.php';
include './constans.php';

$path = ("../uploads/");
$idBarang = $_POST['id_barang'];

$query = "DELETE FROM `$products`  WHERE `product_id` = '$idBarang'";

$responses = array("code" => null, "data" => null, "message" => null);

$resultToken = checkToken();

if ($resultToken['data']->role == 2) {
    header('Content-Type: application/json', true, 400);
    $responses['code'] = 400;
    $responses['message'] = "Akses Ditolak kecuali Admin";
    echo json_encode($responses);
    return;
}

if ($resultToken['status']) {
    $product = get("SELECT COUNT(*) AS count, `image_url` FROM `$products` WHERE `product_id` = '$idBarang'");
    // echo json_encode(file_exists($path . $product->image_url));
    // return;
    if ($product->count == 0) {
        header('Content-Type: application/json', true, 404);
        $responses["code"] = 404;
        $responses["message"] = "Data Tidak Data";
        echo json_encode($responses);
        return;
    }

    $hasil = mysqli_query($con, $query);
    $resUnlink;
    if (file_exists($path . $product->image_url)) {
        $resUnlink = unlink($path . $product->image_url);
    }

    $responses["file"] = $resUnlink;

    if ($hasil) {


        header('Content-Type: application/json', true, 200);
        $responses["code"] = 200;
        $responses["message"] = "Berhasil Hapus Data";
    } else {
        header('Content-Type: application/json', true, 400);
        $responses["code"] = 400;
        $responses["message"] = "Gagal Hapus Data";
    }
} else {
    header('Content-Type: application/json', true, $resultToken['code']);

    $responses['code'] = $resultToken['code'];
    $responses['message'] = $resultToken['msg'];
}

echo json_encode($responses);

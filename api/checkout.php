<?php

include '../config/functions.php';
include './constans.php';

$responses = array("code" => null, "data" => null, "message" => null);

$namaTable = $cart;
$tableProduk = $products;
$tableJual = $penjualan;
$tableJualDetail = $penjualan_detail;

$resultToken = checkToken();

$user_id = $resultToken['data']->user_id;
$grandTotal = $_POST['grandtotal'];
$nilaiBayar = $_POST['nilaibayar'];
$nilaiKembali = $_POST['nilaikembali'];


if ($user_id != "") {
    //INSERT DATA PENJUALAN
    $hasil = $db->query("INSERT INTO $tableJual VALUES(NULL,'$user_id',CURRENT_TIMESTAMP(),'$grandTotal','$nilaiBayar','$nilaiKembali',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP())");

    if ($hasil) {
        $faktur_id = "0";
        $sqlF = "SELECT IFNULL(faktur_id,0) faktur_id FROM $tableJual ORDER BY faktur_id DESC LIMIT 1";
        $rssqlF = mysqli_query($con, $sqlF);

        while ($f = mysqli_fetch_array($rssqlF)) {
            $faktur_id = $f['faktur_id'];
        }

        //INSERT DETAIL PENJUALAN
        $rssql = "SELECT a.cart_id AS cart_id, a.product_id AS product_id, a.qty AS qty, a.price AS price, b.product_name AS product_name FROM $namaTable a JOIN $tableProduk b ON a.product_id = b.product_id WHERE a.user_id = $user_id";
        $sql = mysqli_query($con, $rssql);
        $resultSql = get($rssql);

        if (!is_null($resultSql)) {
            $cart_id = $resultSql->cart_id;
            $product_id = $resultSql->product_id;
            $qty = $resultSql->qty;

            $hasilDetail = $db->query("INSERT INTO $tableJualDetail VALUES(NULL,'$faktur_id','$product_id','$qty',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP())");


            if ($hasilDetail) {
                $ddel = $db->query("DELETE FROM $namaTable WHERE cart_id='$cart_id' AND user_id = '$user_id'");
                if ($ddel) {
                    header('Content-Type: application/json', true, 200);

                    $responses['code'] = 200;
                    $responses['message'] = "Berhasil Tambah Data";
                } else {
                    header('Content-Type: application/json', true, 400);

                    $responses['code'] = 400;
                    $responses['message'] = "Gagal Tambah Data";
                }
            } else {
                header('Content-Type: application/json', true, 400);

                $responses['code'] = 400;
                $responses['coder'] = $hasilDetail;
                $responses['message'] = "Data Detail Gagal Disimpan";
            }
        } else {
            header('Content-Type: application/json', true, 404);

            $responses['code'] = 404;
            $responses['message'] = "Data Gagal Disimpan, Not Found data";
        }
    } else {
        header('Content-Type: application/json', true, 400);

        $responses['code'] = 400;
        $responses['message'] = "Data Master Gagal Disimpan";
    }
} else {
    header('Content-Type: application/json', true, 404);

    $responses['code'] = 404;
    $responses['message'] = "user_id is required";
}

echo json_encode($responses);

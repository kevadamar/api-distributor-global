<?php
include '../config/functions.php';
include './constans.php';

header('Content-Type: application/json');

$namaSupplier = $_POST['full_name'];
$phoneNumber = $_POST['phone_number'];
$address = $_POST['address'];

$responses = array("code" => null, "data" => null, "message" => null);


$resultToken = checkToken();

if ($resultToken['data']->role == 2) {
    header('Content-Type: application/json', true, 400);
    $responses['code'] = 400;
    $responses['message'] = "Akses Ditolak kecuali Admin";
    echo json_encode($responses);
    return;
}

if ($resultToken['status']) {

    $hasil = mysqli_query($con, "INSERT INTO `$suppliers` VALUES(NULL, '$namaSupplier','$phoneNumber','$address' , CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP())");

    if ($hasil) {
        header('Content-Type: application/json', true, 201);
        $responses['code'] = 201;
        $responses['message'] = "Berhasil simpan";
    } else {
        header('Content-Type: application/json', true, 400);
        $responses["code"] = 400;
        $responses['message'] = "Gagal simpan";
    }
} else {
    header('Content-Type: application/json', true, $resultToken['code']);

    $responses['code'] = $resultToken['code'];
    $responses['message'] = $resultToken['msg'];
}

// echo ($responses["data"]);
echo json_encode($responses);

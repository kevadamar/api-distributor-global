<?php
include '../config/functions.php';
include './constans.php';

$id = $_GET['unit_id'];

$query = "SELECT * FROM `$units` WHERE unit_id = '$id'";

$responses = array("code" => null, "data" => null);
// $idx = 0;
$resultToken = checkToken();

if ($resultToken['data']->role == 2) {
    header('Content-Type: application/json', true, 400);
    $responses['code'] = 400;
    $responses['message'] = "Akses Ditolak kecuali Admin";
    echo json_encode($responses);
    return;
}

if ($resultToken['status']) {

    # code...
    $sql = mysqli_query($con, $query);
    $dbField = mysqli_fetch_assoc($sql);

    $responses["data"] = [
        "id" => $dbField['unit_id'],
        "nama_satuan" => $dbField['unit_name'],
        "satuan" => $dbField['unit']
    ];


    header('Content-Type: application/json', true, 200);
    $responses["code"] = 200;
} else {
    header('Content-Type: application/json', true, $resultToken['code']);

    $responses['code'] = $resultToken['code'];
    $responses['message'] = $resultToken['msg'];
}

echo json_encode($responses);

# api-distributor-global


## List Api:

* [X] Login 
* [X] Register
* [X] Logout
* [X] Generate Token / decode token
* [X] CRUD Category (Master Data)
* [X] CRUD Satuan (Master Data)
* [X] CRUD Supplier (Master Data)
* [X] Get List Product By Category
* [X] Get Product By Category & Product ID
* [X] Create Product
* [X] Update Product By Product ID
* [X] Delete Product By Product ID
* [X] Get List Users
* [X] Get User By Token
* [X] Get User By ID
* [X] Update User By ID
* [X] Delete User By ID
* [X] Add To Cart
* [X] Min QTY Cart
* [X] Count Cart
* [X] List Detail Cart
* [X] Proses Checkout Cart
* [ ] Riwayat Transaksi User
* [ ] .....

## Cara Run Dilocal

* Ganti port,username,password sesuai local masing-masing. file di /config/connect.php

* Pastikan terinstall composer. jika belum dapat [Composer Setup](https://getcomposer.org/download/).

* Jika Sudah Terinstal langsung aja -> "composer install" -> pada terminal

* Okay sudah. selamat running..
